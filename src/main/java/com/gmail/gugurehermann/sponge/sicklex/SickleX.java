package com.gmail.gugurehermann.sponge.sicklex;

import java.util.Optional;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.block.TreeData;
import org.spongepowered.api.data.manipulator.mutable.item.DurabilityData;
import org.spongepowered.api.data.manipulator.mutable.item.EnchantmentData;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.data.type.TreeTypes;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.CauseStackManager.StackFrame;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.event.cause.entity.spawn.SpawnTypes;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.enchantment.EnchantmentTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import com.google.inject.Inject;

class LeafBreakResults {
	public int apples;
	public int saplings;
	public int blocksBroken;
}

@Plugin(id = "sickle-x", name = "SickleX", version = "0.1.0", description = "The upgraded leaf sickle!")
public class SickleX {
	Random rand = new Random();

	@Inject
	private Logger logger;

	@Listener
	public void onStart(GameStartedServerEvent event) {
		logger.info("SickleX is now running.");
	}

	@Listener
	public void onBlockBreak(ChangeBlockEvent.Break event, @Root Player player) {
		event.filter((Predicate<Location<World>>) loc -> {
			Optional<ItemStack> _is;

			if ((_is = player.getItemInHand(HandTypes.MAIN_HAND)).isPresent()) {
				ItemStack item = _is.get();

				if (item.getType() == ItemTypes.GOLDEN_HOE) {
					LeafBreakResults res = new LeafBreakResults();
					breakLeafAround(loc, item, 7, true, res);
					item.get(DurabilityData.class).get().durability().set(item.get(DurabilityData.class).get().durability().get() - res.blocksBroken / 5);

					logger.debug(String.format("%s broke %d leaves (got %d saplings, %d apples).", player.getName(),
							res.blocksBroken, res.saplings, res.apples));
				}
			}

			return true;
		});
	}

	private int breakLeafAround(Location<World> location, ItemStack breaker, int depth, boolean bInitial,
			LeafBreakResults res) {
		if (depth < 0)
			return 0;

		BlockState state = location.getBlock();

		if (state.getType() != BlockTypes.LEAVES && state.getType() != BlockTypes.LEAVES2 && !bInitial)
			return 0;

		else {
			if (state.getType() == BlockTypes.LEAVES || state.getType() == BlockTypes.LEAVES2) {
				breakLeaf(location, breaker, res);
				res.blocksBroken++;
			}

			for (int coord = 0; coord < 3; coord++) {
				double[] c2 = new double[] { 0, 0, 0 };

				c2[coord] = 1;
				breakLeafAround(location.add(c2[0], c2[1], c2[2]), breaker, depth - 1, false, res);

				c2[coord] = -1;
				breakLeafAround(location.add(c2[0], c2[1], c2[2]), breaker, depth - 1, false, res);
			}

			return res.blocksBroken;
		}
	}

	private static double[] fortuneSaplingChanceNormal = new double[] { 0.05, 0.0625, 0.0833, 0.1 };
	private static double[] fortuneSaplingChanceJungle = new double[] { 0.025, 0.0278, 0.03125, 0.0417 };
	private static double[] fortuneAppleChance = new double[] { 0.005, 0.00556, 0.00625, 0.00833 };

	private void breakLeaf(Location<World> location, ItemStack breaker, LeafBreakResults res) {
		TreeData leafType = location.get(TreeData.class).get();

		// get fortune level
		class FortuneCheck implements Consumer<Enchantment> {
			public int fortuneLevel = 0;

			@Override
			public void accept(Enchantment ench) {
				if (ench.getType() == EnchantmentTypes.FORTUNE && ench.getLevel() > fortuneLevel)
					fortuneLevel = ench.getLevel();
			}
		}

		FortuneCheck fc = new FortuneCheck();

		if (breaker.get(EnchantmentData.class).isPresent()) {
			EnchantmentData enchData = breaker.get(EnchantmentData.class).get();
			enchData.enchantments().forEach(fc);
		}

		// get chances
		double saplingDropChance = 0;
		double appleDropChance = 0;

		if (leafType.type().get().getId().equalsIgnoreCase(TreeTypes.JUNGLE.getId()))
			saplingDropChance = fortuneSaplingChanceJungle[fc.fortuneLevel];

		else
			saplingDropChance = fortuneSaplingChanceNormal[fc.fortuneLevel];

		if (leafType.type().get().getId().equalsIgnoreCase(TreeTypes.OAK.getId()))
			appleDropChance = fortuneAppleChance[fc.fortuneLevel];

		World world = location.getExtent();

		// drop stuff
		if (rand.nextDouble() < saplingDropChance) {
			ItemStack sapling = ItemStack.builder().itemType(ItemTypes.SAPLING).build();

			TreeData td = sapling.get(TreeData.class).get();
			sapling.offer(td.type().set(leafType.type().get()));

			Item saplingEntity = (Item) world.createEntity(EntityTypes.ITEM, location.getPosition());
			saplingEntity.offer(Keys.REPRESENTED_ITEM, sapling.createSnapshot());

			try (StackFrame frame = Sponge.getCauseStackManager().pushCauseFrame()) {
				frame.addContext(EventContextKeys.SPAWN_TYPE, SpawnTypes.PLUGIN);
				world.spawnEntity(saplingEntity);
			}

			res.saplings++;
		}

		if (appleDropChance > 0 && rand.nextDouble() < appleDropChance) {
			ItemStack apple = ItemStack.builder().itemType(ItemTypes.APPLE).build();

			Item appleEntity = (Item) world.createEntity(EntityTypes.ITEM, location.getPosition());
			appleEntity.offer(Keys.REPRESENTED_ITEM, apple.createSnapshot());

			try (StackFrame frame = Sponge.getCauseStackManager().pushCauseFrame()) {
				frame.addContext(EventContextKeys.SPAWN_TYPE, SpawnTypes.PLUGIN);
				world.spawnEntity(appleEntity);
			}

			res.apples++;
		}

		// remove block
		location.setBlock(BlockState.builder().blockType(BlockTypes.AIR).build());
	}
}
