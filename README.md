# SickleX

SickleX is a basic SpongeAPI plugin, where any gold hoe, when breaking
a leaf block, will end up breaking many leaf blocks around the broken
one, in a "radius". _Saplings, apples, yay!!_

## Installation

Run:

```
    ./gradlew assemble
```

Then, look into the `build/libs` folder for the output JAR, `SickleX-*.jar`.
Paste that into your SpongeAPI server's pĺugin folder.

## Usage

Simple! Craft a golden hoe. That is your sickle!
